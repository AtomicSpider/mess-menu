package iitm.satan.messmenu.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import iitm.satan.messmenu.MessMenuApp;
import iitm.satan.messmenu.R;
import iitm.satan.messmenu.common.Utils;

/**
 * Created by Sanat Dutta on 8/28/2015.
 */
public class MenuFragment extends Fragment {

    private String TAG = "MenuFragment";

    //Views
    private static RelativeLayout notSyncedContainer;
    private static ScrollView menuContainer;
    private static TextView menuDayTextView, menuMealTextView, menuMessTextView, menuItemsTextView, menuExtraItemsTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        Log.i(TAG, "MenuFragment: onCreateView");

        findViews(view);

        if (!Utils.readSharedPreferences(MessMenuApp.isSynced, false))
            notSyncedContainer.setVisibility(View.VISIBLE);

        return view;
    }

    private void findViews(View view) {
        notSyncedContainer = (RelativeLayout) view.findViewById(R.id.not_synced_container);
        menuContainer = (ScrollView) view.findViewById(R.id.menu_container);
        menuDayTextView = (TextView) view.findViewById(R.id.menu_day_textview);
        menuMealTextView = (TextView) view.findViewById(R.id.menu_meal_textview);
        menuMessTextView = (TextView) view.findViewById(R.id.menu_mess_textview);
        menuItemsTextView = (TextView) view.findViewById(R.id.menu_items_textview);
        menuExtraItemsTextView = (TextView) view.findViewById(R.id.menu_extra_items_textview);
    }

    public static void toggleNotSyncedContainerVisibility(boolean visibilityBoolean) {
        if (visibilityBoolean) notSyncedContainer.setVisibility(View.VISIBLE);
        else notSyncedContainer.setVisibility(View.INVISIBLE);
    }

    public static void displayMessData(String day, String meal, String mess, String items, String extras) {
        menuDayTextView.setText(day);
        menuMealTextView.setText(meal);
        menuMessTextView.setText(mess);

        if (!items.equals("N/A")) menuItemsTextView.setText(items);
        else menuItemsTextView.setText("Menu Not Added Yet");
        if (!extras.equals("N/A")) menuExtraItemsTextView.setText(extras);
        else menuExtraItemsTextView.setText("Extras Not Added Yet");

        menuContainer.setVisibility(View.VISIBLE);
    }
}
