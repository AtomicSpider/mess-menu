package iitm.satan.messmenu.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import iitm.satan.messmenu.R;
import iitm.satan.messmenu.activities.ViewMessActivity;
import iitm.satan.messmenu.common.Utils;

/**
 * Created by Sanat Dutta on 8/28/2015.
 */
public class MessFragment extends Fragment {

    private String TAG = "MessFragment";

    //Interfaces
    private messCardAdapter mAdapter;

    //Views
    private ListView mListView;
    private CircularProgressView fetchingDataProgressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout noNetworkContainer;

    //Data
    public static ArrayList<ParseObject> messObjects = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mess, container, false);

        Log.i(TAG, "MessFragment: onCreateView");

        findViews(view);
        mAdapter = new messCardAdapter();
        mListView.setAdapter(mAdapter);
        setUpSwipeRefreshLayout();

        if (!Utils.isConnectedToInternet(getActivity())) {
            fetchingDataProgressBar.setVisibility(View.GONE);
            noNetworkContainer.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.transparent));

        } else
            getMess();

        return view;
    }

    private void getMess() {
        Log.i(TAG, "Fetching Mess");

        ParseQuery<ParseObject> messObjectsQuery = new ParseQuery("Mess");
        messObjectsQuery.whereEqualTo("active", true);
        messObjectsQuery.addAscendingOrder("createdAt");
        messObjectsQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                fetchingDataProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);

                if (e == null) {
                    if (messObjects == null) messObjects = new ArrayList<ParseObject>();
                    messObjects.clear();
                    messObjects.addAll(list);
                    mSwipeRefreshLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.facebook_background));
                    mAdapter.notifyDataSetChanged();
                } else {
                    Log.e(TAG, "Unable to fetch Mess. Error: " + e.getMessage() + " " + e.getCode());
                    Toast.makeText(getActivity(), "Unable to contact server", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setUpSwipeRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "onRefresh");

                if (!Utils.isConnectedToInternet(getActivity())) {
                    messObjects.clear();
                    mAdapter.notifyDataSetChanged();
                    fetchingDataProgressBar.setVisibility(View.GONE);
                    noNetworkContainer.setVisibility(View.VISIBLE);
                    mSwipeRefreshLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.transparent));
                    mSwipeRefreshLayout.setRefreshing(false);
                } else
                    getMess();
            }
        });
    }

    private void findViews(View view) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.mess_swipe_refresh_layout);
        mListView = (ListView) view.findViewById(R.id.mess_listview);
        fetchingDataProgressBar = (CircularProgressView) view.findViewById(R.id.fetchingDataProgressBar);
        noNetworkContainer = (RelativeLayout) view.findViewById(R.id.no_network_container);
    }

    private class messCardAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private messCardAdapter() {
            mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return messObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return messObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View mView = mLayoutInflater.inflate(R.layout.item_mess, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.messName = (TextView) mView.findViewById(R.id.messName);
            mViewHolder.messLocation = (TextView) mView.findViewById(R.id.messLocation);
            mViewHolder.messCard = (RelativeLayout) mView.findViewById(R.id.messCard);

            mViewHolder.messName.setText(messObjects.get(position).getString("messName"));
            if (!messObjects.get(position).getString("messLocation").equals("N/A"))
                mViewHolder.messLocation.setText(messObjects.get(position).getString("messLocation"));
            else mViewHolder.messLocation.setText("Location not added yet");

            mViewHolder.messCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isConnectedToInternet(getActivity())) {
                        Intent mIntent = new Intent(getActivity(), ViewMessActivity.class);
                        mIntent.putExtra("messCode", messObjects.get(position).getString("messCode"));
                        mIntent.putExtra("messName", messObjects.get(position).getString("messName"));
                        startActivity(mIntent);
                        getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom);
                    } else
                        Toast.makeText(getActivity(), "This feature requires internet. Please turn on the network", Toast.LENGTH_SHORT).show();
                }
            });

            return mView;
        }

        private class ViewHolder {
            private TextView messName;
            private TextView messLocation;
            private RelativeLayout messCard;
        }
    }
}
