package iitm.satan.messmenu.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import iitm.satan.messmenu.R;
import iitm.satan.messmenu.activities.PhotoEnlargeActivity;
import iitm.satan.messmenu.common.SquareImageView;
import iitm.satan.messmenu.common.Utils;
import iitm.satan.messmenu.models.DataPager;

/**
 * Created by Sanat Dutta on 8/28/2015.
 */
public class LogFragment extends Fragment {

    private String TAG = "LogFragment";

    //Interfaces
    private logAdapter mAdapter;

    //Views
    private ListView mListView;
    private CircularProgressView fetchingDataProgressBar, fetchingDataProgressBarBelow;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //Data
    private boolean isDataFetching = false, isEverythingLoaded = false;
    private int LOGS_PER_PAGE = 20;
    private Date lastLoadedDate = null;
    private ArrayList<ParseObject> logObjects = new ArrayList<>();
    private DisplayImageOptions options;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log, container, false);

        Log.i(TAG, "LogFragment: onCreateView");

        buildImageLoaderOptions();

        findViews(view);
        mAdapter = new logAdapter();
        mListView.setAdapter(mAdapter);
        setUpSwipeRefreshLayout();
        getLogs(null);

        return view;
    }

    private void buildImageLoaderOptions() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder_image)
                .showImageForEmptyUri(R.drawable.placeholder_image)
                .showImageOnFail(R.drawable.placeholder_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    private void setUpSwipeRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "onRefresh");
                if (!isDataFetching) {
                    isEverythingLoaded = false;
                    lastLoadedDate = null;
                    getLogs(lastLoadedDate);
                } else {
                    Log.i(TAG, "Data Fetch Ongoing");
                }
            }
        });
    }

    private void getLogs(final Date mDate) {
        if (!Utils.isConnectedToInternet(getActivity())) {
            logObjects.clear();
            mAdapter.notifyDataSetChanged();
            fetchingDataProgressBar.setVisibility(View.GONE);
            fetchingDataProgressBarBelow.setVisibility(View.GONE);
            mSwipeRefreshLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.transparent));
            mSwipeRefreshLayout.setRefreshing(false);
        } else {
            isDataFetching = true;
            Log.i(TAG, "Fetching Logs");
            ParseQuery<ParseObject> logObjectsQuery = new ParseQuery("Log");
            logObjectsQuery.setLimit(LOGS_PER_PAGE);
            logObjectsQuery.addDescendingOrder("createdAt");
            if (mDate != null) logObjectsQuery.whereLessThan("createdAt", mDate);
            logObjectsQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    fetchingDataProgressBar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                    if (e == null) {
                        if (list.size() > 0) {
                            Log.i(TAG, "Log Fetching Finished");
                            mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.white));
                            if (mDate == null) logObjects.clear();
                            logObjects.addAll(list);
                            lastLoadedDate = logObjects.get(logObjects.size() - 1).getCreatedAt();
                            mAdapter.notifyDataSetChanged();
                            fetchingDataProgressBarBelow.setVisibility(View.GONE);
                        } else {
                            isEverythingLoaded = true;
                            mAdapter.notifyDataSetChanged();
                            fetchingDataProgressBarBelow.setVisibility(View.GONE);

                        }
                    } else {
                        Log.e(TAG, "Unable to fetch Logs. Error: " + e);
                        Toast.makeText(getActivity(), "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                    isDataFetching = false;
                }
            });
        }
    }

    private void findViews(View view) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.log_swipe_refresh_layout);
        mListView = (ListView) view.findViewById(R.id.log_listview);
        fetchingDataProgressBar = (CircularProgressView) view.findViewById(R.id.fetchingDataProgressBar);
        fetchingDataProgressBarBelow = (CircularProgressView) view.findViewById(R.id.fetchingDataProgressBarBelow);
    }

    private class logAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private logAdapter() {
            mLayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return logObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return logObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if ((position == (logObjects.size() - 1)) && (logObjects.size() > LOGS_PER_PAGE - 1) && !isEverythingLoaded)
                fetchingDataProgressBarBelow.setVisibility(View.VISIBLE);

            View mView;

            if (logObjects.get(position).getBoolean("isComment")) {
                mView = mLayoutInflater.inflate(R.layout.item_comment_log, parent, false);
            } else if (logObjects.get(position).getBoolean("isPicture")) {
                mView = mLayoutInflater.inflate(R.layout.item_photo_log, parent, false);
            } else {
                mView = mLayoutInflater.inflate(R.layout.item_update_log, parent, false);
            }

            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.mLogImage = (ImageView) mView.findViewById(R.id.logImage);
            mViewHolder.mLogText = (TextView) mView.findViewById(R.id.logText);

            ParseObject tmpObject = logObjects.get(position);

            if (logObjects.get(position).getBoolean("isComment")) {
                mViewHolder.mLogText.setText(Html.fromHtml(doBlue(tmpObject.getString("userName")) + " posted a comment in " + doGreen(tmpObject.getString("messName")) + ": " + tmpObject.getString("comment")));
            } else if (logObjects.get(position).getBoolean("isPicture")) {
                mViewHolder.mLogText.setText(Html.fromHtml(doBlue(tmpObject.getString("userName")) + " posted a photo in " + doGreen(tmpObject.getString("messName"))));
                ImageLoader.getInstance().displayImage(tmpObject.getParseFile("picture").getUrl(), mViewHolder.mLogImage, options);

                mViewHolder.mLogImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        enlargePhoto(position);
                    }
                });
            } else {
                mViewHolder.mLogText.setText(Html.fromHtml(doBlue(tmpObject.getString("userName")) + " updated " + doGreen(tmpObject.getString("messName")) + "'s menu: " + tmpObject.getString("update")));
            }

            // Fetch more data
            if ((position == (logObjects.size() - 1)) && !isDataFetching && !isEverythingLoaded)
                getLogs(lastLoadedDate);

            return mView;
        }

        private class ViewHolder {
            private ImageView mLogImage;
            private TextView mLogText;
        }
    }

    private void enlargePhoto(int position) {
        ArrayList<DataPager> imageUriList = new ArrayList<>();
        imageUriList.clear();

        imageUriList.add(new DataPager(logObjects.get(position).getParseFile("picture").getUrl()));

        Intent mIntent = new Intent(getActivity(), PhotoEnlargeActivity.class);
        mIntent.putExtra("imageUris", imageUriList);
        mIntent.putExtra("position", 0);
        startActivity(mIntent);
    }

    private String doGreen(String mString) {
        return "<strong><font color=\"#009688\">" + mString + "</font></strong>";
    }

    private String doBlue(String mString) {
        return "<strong><font color=\"#1976D2\">" + mString + "</font></strong>";
    }
}
