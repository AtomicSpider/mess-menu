package iitm.satan.messmenu.models;

import java.io.Serializable;

/**
 * Created by Sanat Dutta on 8/31/2015.
 */
public class DataPager implements Serializable {
    private String imageUri = "";

    public DataPager(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getImageUri() {
        return imageUri;
    }
}