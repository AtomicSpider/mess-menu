package iitm.satan.messmenu.models;

/**
 * Created by Sanat Dutta on 8/30/2015.
 */
public class MenuObject {
    private String Day, Meal, MessCode, MessName, Main, Extras;

    public String getDay() {
        return Day;
    }

    public String getMeal() {
        return Meal;
    }

    public String getMessCode() {
        return MessCode;
    }

    public String getMessName() {
        return MessName;
    }

    public String getMain() {
        return Main;
    }

    public String getExtras() {
        return Extras;
    }

    public void setDay(String day) {
        Day = day;
    }

    public void setMeal(String meal) {
        Meal = meal;
    }

    public void setMessCode(String messCode) {
        MessCode = messCode;
    }

    public void setMessName(String messName) {
        MessName = messName;
    }

    public void setMain(String main) {
        Main = main;
    }

    public void setExtras(String extras) {
        Extras = extras;
    }
}
