package iitm.satan.messmenu.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import iitm.satan.messmenu.MessMenuApp;

/**
 * Created by Sanat Dutta on 8/29/2015.
 */
public class Utils {

    private String TAG = "Utils";

    public static void saveToSharedPreferences(String key, String sValue, boolean isBoolean, boolean bValue) {
        SharedPreferences.Editor mEditor = MessMenuApp.mSharedPreferences.edit();
        if (isBoolean) mEditor.putBoolean(key, bValue);
        else mEditor.putString(key, sValue);
        mEditor.commit();
    }

    public static boolean readSharedPreferences(String key, boolean valueDefault) {
        return MessMenuApp.mSharedPreferences.getBoolean(key, valueDefault);
    }

    public static String readSharedPreferences(String key) {
        return MessMenuApp.mSharedPreferences.getString(key, null);
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting());
    }

    public static int dpToPx(int dp, Context mContext) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(int px, Context mContext) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }
}
