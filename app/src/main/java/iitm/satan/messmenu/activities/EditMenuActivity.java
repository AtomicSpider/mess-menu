package iitm.satan.messmenu.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import iitm.satan.messmenu.R;

/**
 * Created by Sanat Dutta on 8/29/2015.
 */
public class EditMenuActivity extends AppCompatActivity {

    private String TAG = "EditMenuActivity";

    //Interface

    //Views
    private Spinner messSpinner, mealSpinner, daySpinner;
    private EditText mainEditText, extrasEditText;
    private MaterialDialog progressDialog;
    private ScrollView mainContainer;

    //Data
    private ArrayList<String> messArrayList, messCodeArrayList;
    private List<ParseObject> messListServer;
    private ParseObject tempSingleMenu;
    private int messSpinnerPos = 0, mealSpinnerPos = 0, daySpinnerPos = 0;
    private boolean autoFetch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "EditMenuActivity: onCreate()");

        setContentView(R.layout.activity_edit_menu);
        findViews();
        setUpActionBar();

        fetchMessList();

    }

    private void fetchMessList() {
        showProgressDialog("Fetching Mess From Server...");

        ParseQuery<ParseObject> messListServerQuery = ParseQuery.getQuery("Mess");
        messListServerQuery.whereEqualTo("active", true);
        messListServerQuery.addAscendingOrder("createdAt");
        messListServerQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    Log.i(TAG, list.size() + " messes found on server");
                    if (messListServer == null) messListServer = new ArrayList<ParseObject>();
                    messListServer = list;

                    fetchDataFromServer(MainScreenActivity.currentMenu.getMessCode(), MainScreenActivity.currentMenu.getDay(), MainScreenActivity.currentMenu.getMeal());
                    setUpSpinner();
                    setUpSpinnerListener();
                    mainContainer.setVisibility(View.VISIBLE);
                } else {
                    Log.i(TAG, "Mess fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(EditMenuActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void fetchDataFromServer(String messCode, String day, final String meal) {
        showProgressDialog("Fetching Data From Server...");

        ParseQuery<ParseObject> singleMenuQuery = ParseQuery.getQuery("Menu");
        singleMenuQuery.whereEqualTo("messCode", messCode);
        singleMenuQuery.whereEqualTo("day", day);
        singleMenuQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                progressDialog.dismiss();
                if (!autoFetch) autoFetch = true;
                if (e == null) {
                    Log.i(TAG, "Single Menu fetched");

                    if (list.size() != 1) {
                        Log.i(TAG, "Multiple Menu found. List Size: " + list.size());
                        Toast.makeText(EditMenuActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        if (meal.equals("Breakfast")) {
                            if (!list.get(0).getString("breakfastMain").equals("N/A"))
                                mainEditText.setText(list.get(0).getString("breakfastMain"));
                            else mainEditText.setText("");
                            if (!list.get(0).getString("breakfastExtras").equals("N/A"))
                                extrasEditText.setText(list.get(0).getString("breakfastExtras"));
                            else extrasEditText.setText("");
                        } else if (meal.equals("Lunch")) {
                            if (!list.get(0).getString("lunchMain").equals("N/A"))
                                mainEditText.setText(list.get(0).getString("lunchMain"));
                            else mainEditText.setText("");
                            if (!list.get(0).getString("lunchExtras").equals("N/A"))
                                extrasEditText.setText(list.get(0).getString("lunchExtras"));
                            else extrasEditText.setText("");
                        } else if (meal.equals("Dinner")) {
                            if (!list.get(0).getString("dinnerMain").equals("N/A"))
                                mainEditText.setText(list.get(0).getString("dinnerMain"));
                            else mainEditText.setText("");
                            if (!list.get(0).getString("dinnerExtras").equals("N/A"))
                                extrasEditText.setText(list.get(0).getString("dinnerExtras"));
                            else extrasEditText.setText("");
                        }
                        list.get(0).pinInBackground();
                        tempSingleMenu = list.get(0);
                    }
                } else {
                    Log.i(TAG, "Single Menu fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(EditMenuActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setUpSpinnerListener() {
        messSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                messSpinnerPos = position;
                if (autoFetch) {
                    fetchDataFromServer(messCodeArrayList.get(messSpinnerPos), MainScreenActivity.dayArray.get(daySpinnerPos), MainScreenActivity.mealArray.get(mealSpinnerPos));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mealSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mealSpinnerPos = position;
                if (autoFetch) {
                    fetchDataFromServer(messCodeArrayList.get(messSpinnerPos), MainScreenActivity.dayArray.get(daySpinnerPos), MainScreenActivity.mealArray.get(mealSpinnerPos));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                daySpinnerPos = position;
                if (autoFetch) {
                    fetchDataFromServer(messCodeArrayList.get(messSpinnerPos), MainScreenActivity.dayArray.get(daySpinnerPos), MainScreenActivity.mealArray.get(mealSpinnerPos));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpSpinner() {

        if (messArrayList == null) messArrayList = new ArrayList<>();
        if (messCodeArrayList == null) messCodeArrayList = new ArrayList<>();
        messArrayList.clear();
        messCodeArrayList.clear();

        for (int i = 0; i < messListServer.size(); i++) {
            messArrayList.add(messListServer.get(i).getString("messName"));
            messCodeArrayList.add(messListServer.get(i).getString("messCode"));
        }

        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, messArrayList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        messSpinner.setAdapter(mAdapter);
        if (messCodeArrayList.indexOf(MainScreenActivity.currentMenu.getMessCode()) != -1)
            messSpinner.setSelection(messCodeArrayList.indexOf(MainScreenActivity.currentMenu.getMessCode()));
        else {
            Toast.makeText(EditMenuActivity.this, "Your current mess is not active", Toast.LENGTH_LONG).show();
            messSpinner.setSelection(0);
        }

        ArrayAdapter<CharSequence> mAdapter1 = ArrayAdapter.createFromResource(this,
                R.array.meals, R.layout.item_spinner);
        mAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mealSpinner.setAdapter(mAdapter1);
        if (MainScreenActivity.currentMenu.getMeal().equals("Breakfast"))
            mealSpinner.setSelection(0);
        else if (MainScreenActivity.currentMenu.getMeal().equals("Lunch"))
            mealSpinner.setSelection(1);
        else if (MainScreenActivity.currentMenu.getMeal().equals("Dinner"))
            mealSpinner.setSelection(2);

        ArrayAdapter<CharSequence> mAdapter2 = ArrayAdapter.createFromResource(this,
                R.array.days, R.layout.item_spinner);
        mAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(mAdapter2);
        if (MainScreenActivity.currentMenu.getDay().equals("Monday")) daySpinner.setSelection(0);
        else if (MainScreenActivity.currentMenu.getDay().equals("Tuesday"))
            daySpinner.setSelection(1);
        else if (MainScreenActivity.currentMenu.getDay().equals("Wednesday"))
            daySpinner.setSelection(2);
        else if (MainScreenActivity.currentMenu.getDay().equals("Thursday"))
            daySpinner.setSelection(3);
        else if (MainScreenActivity.currentMenu.getDay().equals("Friday"))
            daySpinner.setSelection(4);
        else if (MainScreenActivity.currentMenu.getDay().equals("Saturday"))
            daySpinner.setSelection(5);
        else if (MainScreenActivity.currentMenu.getDay().equals("Sunday"))
            daySpinner.setSelection(6);
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_save:
                saveCurrentSingleMenu();
                break;
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    private void saveCurrentSingleMenu() {
        if (tempSingleMenu == null) {
            Toast.makeText(EditMenuActivity.this, "Oops... something went wrong.", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            final String mainText, extraText;
            if (mainEditText.getText().toString().trim().equals("")) mainText = "N/A";
            else mainText = mainEditText.getText().toString().trim();
            if (extrasEditText.getText().toString().trim().equals("")) extraText = "N/A";
            else extraText = extrasEditText.getText().toString().trim();

            switch (mealSpinnerPos) {
                case 0:
                    tempSingleMenu.put("breakfastMain", mainText);
                    tempSingleMenu.put("breakfastExtras", extraText);
                    break;
                case 1:
                    tempSingleMenu.put("lunchMain", mainText);
                    tempSingleMenu.put("lunchExtras", extraText);
                    break;
                case 2:
                    tempSingleMenu.put("dinnerMain", mainText);
                    tempSingleMenu.put("dinnerExtras", extraText);
                    break;
            }

            showProgressDialog("Updating Menu...");
            tempSingleMenu.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        ParseObject logAsset = new ParseObject("Log");
                        logAsset.put("messCode", tempSingleMenu.getString("messCode"));
                        logAsset.put("messName", tempSingleMenu.getString("messName"));
                        if (MainScreenActivity.currentUserName != null)
                            logAsset.put("userName", MainScreenActivity.currentUserName);
                        else logAsset.put("userName", "Anonymous");
                        logAsset.put("isUpdate", true);
                        logAsset.put("update", MainScreenActivity.dayArray.get(daySpinnerPos) + " ► " + MainScreenActivity.mealArray.get(mealSpinnerPos));
                        logAsset.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                progressDialog.dismiss();
                                Toast.makeText(EditMenuActivity.this, "Menu Updated.", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(EditMenuActivity.this, "Oops... something went wrong.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            tempSingleMenu.pinInBackground();
        }
    }

    private void findViews() {
        mainContainer = (ScrollView) findViewById(R.id.main_container);
        messSpinner = (Spinner) findViewById(R.id.messSpinner);
        mealSpinner = (Spinner) findViewById(R.id.mealSpinner);
        daySpinner = (Spinner) findViewById(R.id.daySpinner);
        mainEditText = (EditText) findViewById(R.id.mainEditText);
        extrasEditText = (EditText) findViewById(R.id.extrasEditText);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }
}
