package iitm.satan.messmenu.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import iitm.satan.messmenu.R;

/**
 * Created by Sanat Dutta on 8/31/2015.
 */
public class ViewCommentsActivity extends AppCompatActivity {

    private String TAG = "ViewCommentsActivity";

    //Interfaces
    private commentsAdapter mAdapter;

    //Views
    private ListView mListView;
    private CircularProgressView fetchingDataProgressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //Data
    private String messCode, messName;
    private boolean isDataFetching = false, isEverythingLoaded = false;
    private int COMMENTS_PER_PAGE = 20;
    private Date lastLoadedDate = null;
    private ArrayList<ParseObject> commentObjects = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "ViewCommentsActivity: onCreateView");
        setContentView(R.layout.activity_view_comments);

        getIntentData();
        findViews();
        setUpActionBar();
        mAdapter = new commentsAdapter();
        mListView.setAdapter(mAdapter);
        setUpSwipeRefreshLayout();
        getComments(null);
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(messName + " Comments");
    }

    private void getIntentData() {
        messCode = getIntent().getStringExtra("messCode");
        messName = getIntent().getStringExtra("messName");
    }

    private void getComments(final Date mDate) {
        isDataFetching = true;

        Log.i(TAG, "Fetching Comments");
        ParseQuery<ParseObject> commentObjectsQuery = new ParseQuery("MessAssets");
        commentObjectsQuery.setLimit(COMMENTS_PER_PAGE);
        commentObjectsQuery.addDescendingOrder("createdAt");
        commentObjectsQuery.whereEqualTo("messCode", messCode);
        commentObjectsQuery.whereEqualTo("isComment", true);
        if (mDate != null) commentObjectsQuery.whereLessThan("createdAt", mDate);
        commentObjectsQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                fetchingDataProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                if (e == null) {
                    if (list.size() > 0) {
                        Log.i(TAG, "Comments Fetching Finished");
                        mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.white));
                        if (mDate == null) commentObjects.clear();
                        commentObjects.addAll(list);
                        lastLoadedDate = commentObjects.get(commentObjects.size() - 1).getCreatedAt();
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (mDate == null) {
                            Log.e(TAG, "No Recent Comments");
                            commentObjects.clear();
                            mAdapter.notifyDataSetChanged();
                            mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else {
                            isEverythingLoaded = true;
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    Log.e(TAG, "Unable to fetch Comments. Error: " + e);
                    Toast.makeText(ViewCommentsActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                }
                isDataFetching = false;
            }
        });
    }

    private void setUpSwipeRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "onRefresh");
                if (!isDataFetching) {
                    isEverythingLoaded = false;
                    lastLoadedDate = null;
                    getComments(lastLoadedDate);
                } else {
                    Log.i(TAG, "Data Fetch Ongoing");
                }
            }
        });
    }

    private void findViews() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.comments_swipe_refresh_layout);
        mListView = (ListView) findViewById(R.id.comments_listview);
        fetchingDataProgressBar = (CircularProgressView) findViewById(R.id.fetchingDataProgressBar);
    }

    private class commentsAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private commentsAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return commentObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return commentObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View mView;
            if ((position == (commentObjects.size() - 1)) && (commentObjects.size() > COMMENTS_PER_PAGE - 1) && !isEverythingLoaded)
                mView = mLayoutInflater.inflate(R.layout.item_comment_activity_last, parent, false);
            else mView = mLayoutInflater.inflate(R.layout.item_comment_activity, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.userName = (TextView) mView.findViewById(R.id.userName);
            mViewHolder.messName = (TextView) mView.findViewById(R.id.messName);
            mViewHolder.userComment = (TextView) mView.findViewById(R.id.userComment);

            mViewHolder.userName.setText(commentObjects.get(position).getString("userName"));
            mViewHolder.messName.setText(commentObjects.get(position).getString("messName"));
            mViewHolder.userComment.setText(commentObjects.get(position).getString("comment"));

            // Fetch more data
            if ((position == (commentObjects.size() - 1)) && !isDataFetching && !isEverythingLoaded)
                getComments(lastLoadedDate);

            return mView;
        }

        private class ViewHolder {
            private TextView userName;
            private TextView messName;
            private TextView userComment;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

