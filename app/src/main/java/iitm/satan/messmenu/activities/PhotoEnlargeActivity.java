package iitm.satan.messmenu.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import iitm.satan.messmenu.R;
import iitm.satan.messmenu.common.TouchImageView;
import iitm.satan.messmenu.models.DataPager;
import iitm.satan.messmenu.models.DepthPageTransformer;
import iitm.satan.messmenu.models.ExtendedViewPager;

/**
 * Created by Sanat Dutta on 8/30/2015.
 */
public class PhotoEnlargeActivity extends AppCompatActivity {

    private String TAG = "PhotoEnlargeActivity";

    //Interface
    private ExtendedViewPager mViewPager;
    private CustomPageAdapter mAdapter;

    //View
    private MaterialDialog progressDialog;

    //Data
    private DisplayImageOptions options;
    private ArrayList<DataPager> imageUris;
    private int currentPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "PhotoEnlargeActivity: onCreate()");

        setContentView(R.layout.activity_photo_enlarge);

        getIntentData();

        setUpActionBar();
        setUpViewPager();
        setUpViewPagerOnClickListener();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.photo_enlarge_placeholder_image)
                .showImageForEmptyUri(R.drawable.photo_enlarge_placeholder_image)
                .showImageOnFail(R.drawable.photo_enlarge_placeholder_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    private void setUpViewPagerOnClickListener() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getSupportActionBar().show();
                currentPosition = position;
                getSupportActionBar().setTitle("Mess Menu "+String.valueOf(position + 1) + "/" + String.valueOf(imageUris.size()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUpViewPager() {
        mViewPager = (ExtendedViewPager) findViewById(R.id.pager);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mAdapter = new CustomPageAdapter(this);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(currentPosition);
    }

    private void getIntentData() {
        imageUris = new ArrayList<>();
        imageUris = (ArrayList<DataPager>) getIntent().getSerializableExtra("imageUris");
        currentPosition = getIntent().getExtras().getInt("position");
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Mess Menu "+String.valueOf(currentPosition + 1) + "/" + String.valueOf(imageUris.size()));
    }

    private class CustomPageAdapter extends PagerAdapter {

        private Context mContext;
        private LayoutInflater mLayoutInflater;

        public CustomPageAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getItemPosition(Object object) {
            //return super.getItemPosition(object);
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return imageUris.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.touch_image_view, container, false);
            TouchImageView mTouchImageView = (TouchImageView) itemView.findViewById(R.id.photoImage);
            String mString = imageUris.get(position).getImageUri();

            ImageLoader.getInstance().displayImage(mString, mTouchImageView, options);

            mTouchImageView.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                int flag_zoom = 0;

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    if (getSupportActionBar().isShowing()) {
                        getSupportActionBar().hide();
                    } else {
                        getSupportActionBar().show();
                    }

                    return false;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    if (flag_zoom == 0) {
                        getSupportActionBar().hide();
                        flag_zoom = 1;
                    } else {
                        flag_zoom = 0;
                        getSupportActionBar().show();
                    }
                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    return false;
                }
            });

            container.addView(itemView, 0);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
            View view = (View) object;
            view = null;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}