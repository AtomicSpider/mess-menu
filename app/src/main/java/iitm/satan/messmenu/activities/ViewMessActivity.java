package iitm.satan.messmenu.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import iitm.satan.messmenu.R;
import iitm.satan.messmenu.common.SquareImageView;
import iitm.satan.messmenu.common.Utils;
import iitm.satan.messmenu.fragments.MessFragment;
import iitm.satan.messmenu.models.DataPager;

/**
 * Created by Sanat Dutta on 8/30/2015.
 */
public class ViewMessActivity extends AppCompatActivity {

    private String TAG = "ViewMessActivity";

    private int CHOOSE_IMAGE_FROM_CAMERA = 11;
    private int CHOOSE_IMAGE_FROM_GALLERY = 12;

    //Interfaces

    //Views
    private EditText commentMessEditText;
    private ImageView commentSend;
    private MaterialDialog progressDialog;
    private TextView noCommentTextView, noPhotoTextView, addPhoto;
    private MaterialDialog progressBar;
    private SquareImageView thumb1, thumb2, thumb3;
    private LinearLayout thumbContainer, viewAllComments, viewAllPhotos;
    private LinearLayout commentLayout, commentView1, commentView2, commentView3;
    private TextView userName1, userName2, userName3, messName1, messName2, messName3, userComment1, userComment2, userComment3;
    private View positiveAction;

    //Data
    private String messCode, messName;
    private String locationEditText;
    private List<ParseObject> commentObjects = new ArrayList<>(), photoObjects = new ArrayList<>();
    private boolean isMessCommentReady = false, isMessPhotoReady = false;
    private Uri mImageUri;
    private ParseFile mParseFile;
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "ViewMessActivity: onCreate()");

        getIntentData();

        setContentView(R.layout.activity_view_mess);
        findViews();

        setUpActionBar();

        setUpOnClickListeners();

        fetchMessData();
    }

    private void fetchMessData() {
        showProgressDialog("Fetching Mess Data...");

        ParseQuery<ParseObject> messCommentListQuery = ParseQuery.getQuery("MessAssets");
        messCommentListQuery.addDescendingOrder("createdAt");
        messCommentListQuery.whereEqualTo("messCode", messCode);
        messCommentListQuery.setLimit(3);
        messCommentListQuery.whereEqualTo("isComment", true);
        messCommentListQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> messCommentList, ParseException e) {
                if (e == null) {
                    Log.i(TAG, "Mess Comment List fetched");
                    isMessCommentReady = true;
                    if (commentObjects == null) commentObjects = new ArrayList<ParseObject>();
                    commentObjects.clear();
                    commentObjects.addAll(messCommentList);

                    if (isMessCommentReady && isMessPhotoReady) {
                        progressDialog.dismiss();
                        displayCommentsAndPhotos(2);
                    }

                } else {
                    Log.i(TAG, "Mess Comment List fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(ViewMessActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ParseQuery<ParseObject> messPhotoListQuery = ParseQuery.getQuery("MessAssets");
        messPhotoListQuery.addDescendingOrder("createdAt");
        messPhotoListQuery.setLimit(3);
        messPhotoListQuery.whereEqualTo("messCode", messCode);
        messPhotoListQuery.whereEqualTo("isPicture", true);
        messPhotoListQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> messPhotoList, ParseException e) {
                if (e == null) {
                    Log.i(TAG, "Mess Photo List fetched");
                    isMessPhotoReady = true;
                    if (photoObjects == null) photoObjects = new ArrayList<ParseObject>();
                    photoObjects.clear();
                    photoObjects.addAll(messPhotoList);

                    if (isMessCommentReady && isMessPhotoReady) {
                        progressDialog.dismiss();
                        displayCommentsAndPhotos(2);
                    }

                } else {
                    Log.i(TAG, "Mess Photo List fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(ViewMessActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void displayCommentsAndPhotos(int displayStatus) {
        if (displayStatus == 0 | displayStatus == 2) {
            Log.i(TAG, "displayComments()");
            if (commentObjects.size() == 0) {
                Log.i(TAG, "displayComments(): Comments:0");
                noCommentTextView.setVisibility(View.VISIBLE);
                commentLayout.setVisibility(View.GONE);
            } else {
                switch (commentObjects.size()) {
                    case 1:
                        commentView1.setVisibility(View.VISIBLE);
                        commentView2.setVisibility(View.GONE);
                        commentView3.setVisibility(View.GONE);
                        userName1.setText(commentObjects.get(0).getString("userName"));
                        messName1.setText(commentObjects.get(0).getString("messName"));
                        userComment1.setText(commentObjects.get(0).getString("comment"));
                        break;
                    case 2:
                        commentView1.setVisibility(View.VISIBLE);
                        commentView2.setVisibility(View.VISIBLE);
                        commentView3.setVisibility(View.GONE);
                        userName1.setText(commentObjects.get(0).getString("userName"));
                        messName1.setText(commentObjects.get(0).getString("messName"));
                        userComment1.setText(commentObjects.get(0).getString("comment"));
                        userName2.setText(commentObjects.get(1).getString("userName"));
                        messName2.setText(commentObjects.get(1).getString("messName"));
                        userComment2.setText(commentObjects.get(1).getString("comment"));
                        break;
                    case 3:
                        commentView1.setVisibility(View.VISIBLE);
                        commentView2.setVisibility(View.VISIBLE);
                        commentView3.setVisibility(View.VISIBLE);
                        userName1.setText(commentObjects.get(0).getString("userName"));
                        messName1.setText(commentObjects.get(0).getString("messName"));
                        userComment1.setText(commentObjects.get(0).getString("comment"));
                        userName2.setText(commentObjects.get(1).getString("userName"));
                        messName2.setText(commentObjects.get(1).getString("messName"));
                        userComment2.setText(commentObjects.get(1).getString("comment"));
                        userName3.setText(commentObjects.get(2).getString("userName"));
                        messName3.setText(commentObjects.get(2).getString("messName"));
                        userComment3.setText(commentObjects.get(2).getString("comment"));
                        break;

                }
                noCommentTextView.setVisibility(View.GONE);
                commentLayout.setVisibility(View.VISIBLE);
            }
        }
        if (displayStatus == 1 | displayStatus == 2) {
            Log.i(TAG, "displayPictures()");

            if (photoObjects.size() > 0) {
                options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.placeholder_image)
                        .showImageForEmptyUri(R.drawable.placeholder_image)
                        .showImageOnFail(R.drawable.placeholder_image)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .build();
            }

            switch (photoObjects.size()) {
                case 0:
                    Log.i(TAG, "displayPictures(): Photo:0");
                    thumbContainer.setVisibility(View.GONE);
                    noPhotoTextView.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    Log.i(TAG, "displayPictures(): Photo:1");
                    thumb1.setVisibility(View.VISIBLE);
                    thumb2.setVisibility(View.GONE);
                    thumb3.setVisibility(View.GONE);
                    thumbContainer.setVisibility(View.VISIBLE);
                    noPhotoTextView.setVisibility(View.GONE);
                    ImageLoader.getInstance().displayImage(photoObjects.get(0).getParseFile("picture").getUrl(), thumb1, options);
                    thumb1.setVisibility(View.VISIBLE);
                    thumb2.setVisibility(View.GONE);
                    thumb3.setVisibility(View.GONE);
                    break;
                case 2:
                    Log.i(TAG, "displayPictures(): Photo:2");
                    thumb1.setVisibility(View.VISIBLE);
                    thumb2.setVisibility(View.VISIBLE);
                    thumb3.setVisibility(View.GONE);
                    thumbContainer.setVisibility(View.VISIBLE);
                    noPhotoTextView.setVisibility(View.GONE);
                    ImageLoader.getInstance().displayImage(photoObjects.get(0).getParseFile("picture").getUrl(), thumb1, options);
                    ImageLoader.getInstance().displayImage(photoObjects.get(1).getParseFile("picture").getUrl(), thumb2, options);
                    break;
                case 3:
                    Log.i(TAG, "displayPictures(): Photo:3");
                    thumb1.setVisibility(View.VISIBLE);
                    thumb2.setVisibility(View.VISIBLE);
                    thumb3.setVisibility(View.VISIBLE);
                    thumbContainer.setVisibility(View.VISIBLE);
                    noPhotoTextView.setVisibility(View.GONE);
                    ImageLoader.getInstance().displayImage(photoObjects.get(0).getParseFile("picture").getUrl(), thumb1, options);
                    ImageLoader.getInstance().displayImage(photoObjects.get(1).getParseFile("picture").getUrl(), thumb2, options);
                    ImageLoader.getInstance().displayImage(photoObjects.get(2).getParseFile("picture").getUrl(), thumb3, options);
                    break;
            }
        }
    }

    private void getIntentData() {
        messCode = getIntent().getStringExtra("messCode");
        messName = getIntent().getStringExtra("messName");
    }

    private void setUpOnClickListeners() {
        commentSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = commentMessEditText.getText().toString().trim();


                if (!comment.equals("")) {
                    showProgressDialog("Submitting Comment...");

                    final ParseObject messAsset = new ParseObject("MessAssets");
                    messAsset.put("messCode", messCode);
                    messAsset.put("messName", messName);
                    if (MainScreenActivity.currentUserName != null)
                        messAsset.put("userName", MainScreenActivity.currentUserName);
                    else messAsset.put("userName", "Anonymous");
                    messAsset.put("isComment", true);
                    messAsset.put("comment", comment);
                    messAsset.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            progressDialog.dismiss();
                            commentMessEditText.setText("");
                            if (e == null) {
                                Toast.makeText(ViewMessActivity.this, "Comment posted", Toast.LENGTH_SHORT).show();
                                ParseObject logAsset = new ParseObject("Log");
                                logAsset.put("messCode", messCode);
                                logAsset.put("messName", messName);
                                logAsset.put("userName", messAsset.getString("userName"));
                                logAsset.put("isComment", true);
                                logAsset.put("comment", messAsset.getString("comment"));
                                logAsset.saveInBackground();

                                commentObjects.add(0, messAsset);
                                if (commentObjects.size() == 4) commentObjects.remove(3);
                                displayCommentsAndPhotos(0);
                            } else
                                Toast.makeText(ViewMessActivity.this, "Oops, something went wrong.", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else
                    Toast.makeText(ViewMessActivity.this, "Please write a comment", Toast.LENGTH_SHORT).show();
            }
        });

        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog dialog = new MaterialDialog.Builder(ViewMessActivity.this)
                        .title("Choose Picture")
                        .titleColorRes(R.color.teal_700)
                        .customView(R.layout.dialog_picture_chooser, true)
                        .show();

                View view = dialog.getCustomView();

                TextView fromCamera = (TextView) view.findViewById(R.id.fromCamera);
                TextView fromGallery = (TextView) view.findViewById(R.id.fromGallery);

                fromCamera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        chooseImage(true);
                    }
                });

                fromGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        chooseImage(false);
                    }
                });
            }
        });

        thumb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enlargePhoto(0);
            }
        });
        thumb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enlargePhoto(1);
            }
        });
        thumb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enlargePhoto(2);
            }
        });

        viewAllComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.isConnectedToInternet(ViewMessActivity.this))
                    Toast.makeText(ViewMessActivity.this, "This feature requires internet. Please turn on the network", Toast.LENGTH_SHORT).show();
                else {
                    Intent mIntent = new Intent(ViewMessActivity.this, ViewCommentsActivity.class);
                    mIntent.putExtra("messCode", messCode);
                    mIntent.putExtra("messName", messName);
                    startActivity(mIntent);
                }
            }
        });

        viewAllPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.isConnectedToInternet(ViewMessActivity.this))
                    Toast.makeText(ViewMessActivity.this, "This feature requires internet. Please turn on the network", Toast.LENGTH_SHORT).show();
                else {
                    Intent mIntent = new Intent(ViewMessActivity.this, ViewPhotosActivity.class);
                    mIntent.putExtra("messCode", messCode);
                    mIntent.putExtra("messName", messName);
                    startActivity(mIntent);
                }
            }
        });
    }

    private void enlargePhoto(int position) {
        ArrayList<DataPager> imageUriList = new ArrayList<>();
        imageUriList.clear();

        for (int i = 0; i < photoObjects.size(); i++) {
            imageUriList.add(new DataPager(photoObjects.get(i).getParseFile("picture").getUrl()));
        }

        Intent mIntent = new Intent(ViewMessActivity.this, PhotoEnlargeActivity.class);
        mIntent.putExtra("imageUris", imageUriList);
        mIntent.putExtra("position", position);
        startActivity(mIntent);
    }

    private void findViews() {
        commentMessEditText = (EditText) findViewById(R.id.comment_mess_edit_text);
        commentSend = (ImageView) findViewById(R.id.comment_send);
        noCommentTextView = (TextView) findViewById(R.id.noCommentTextView);
        addPhoto = (TextView) findViewById(R.id.addPhoto);
        thumb1 = (SquareImageView) findViewById(R.id.thumb1);
        thumb2 = (SquareImageView) findViewById(R.id.thumb2);
        thumb3 = (SquareImageView) findViewById(R.id.thumb3);
        thumbContainer = (LinearLayout) findViewById(R.id.thumbContainer);
        noPhotoTextView = (TextView) findViewById(R.id.noPhotoTextView);
        viewAllComments = (LinearLayout) findViewById(R.id.viewAllComments);
        viewAllPhotos = (LinearLayout) findViewById(R.id.viewAllPhotos);
        commentLayout = (LinearLayout) findViewById(R.id.commentLayout);
        commentView1 = (LinearLayout) findViewById(R.id.commentView1);
        commentView2 = (LinearLayout) findViewById(R.id.commentView2);
        commentView3 = (LinearLayout) findViewById(R.id.commentView3);
        userName1 = (TextView) findViewById(R.id.userName1);
        userName2 = (TextView) findViewById(R.id.userName2);
        userName3 = (TextView) findViewById(R.id.userName3);
        messName1 = (TextView) findViewById(R.id.messName1);
        messName2 = (TextView) findViewById(R.id.messName2);
        messName3 = (TextView) findViewById(R.id.messName3);
        userComment1 = (TextView) findViewById(R.id.userComment1);
        userComment2 = (TextView) findViewById(R.id.userComment2);
        userComment3 = (TextView) findViewById(R.id.userComment3);
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(messName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_mess, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_set_location:
                getMessObject(messCode);
                break;
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    private void getMessObject(String messCode) {
        showProgressDialog("Fetching current location...");
        ParseQuery<ParseObject> messQuery = ParseQuery.getQuery("Mess");
        messQuery.whereEqualTo("messCode", messCode);
        messQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    if (list.size() != 1) {
                        Log.i(TAG, "Multiple mess found");
                        Toast.makeText(ViewMessActivity.this, "Oops, something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        setMessLocation(list.get(0));
                    }
                } else {
                    Log.i(TAG, "Can't get mess object. Error: " + e.getCode() + " " + e.getMessage());
                    Toast.makeText(ViewMessActivity.this, "Oops, something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setMessLocation(final ParseObject currentMessObject) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Enter Mess Location")
                .titleColorRes(R.color.teal_700)
                .customView(R.layout.dialog_set_mess_location, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        showProgressDialog("Submitting...");

                        ParseObject tempMessObject = currentMessObject;
                        tempMessObject.put("messLocation", locationEditText);
                        tempMessObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                progressDialog.dismiss();
                                if (e == null) {
                                    Toast.makeText(ViewMessActivity.this, "Location Set.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Log.i(TAG, "Could not save mess location. Error: " + e.getCode() + " " + e.getMessage());
                                    Toast.makeText(ViewMessActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        EditText locationInput = (EditText) dialog.getCustomView().findViewById(R.id.messLocationEditText);
        if (!currentMessObject.getString("messLocation").equals("N/A"))
            locationInput.setText(currentMessObject.getString("messLocation"));
        else locationInput.setText("");
        locationInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    locationEditText = s.toString().trim();
                    positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.teal_700)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void showProgressBar(String message) {
        progressBar = new MaterialDialog.Builder(this)
                .title(message)
                .progress(false, 100, false)
                .show();

    }

    private boolean chooseImage(boolean fromCamera) {
        boolean Error = false;
        if (fromCamera) {

            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            File photo = null;
            try {
                // place where to store camera taken picture
                photo = this.createTemporaryFile("picture", ".jpg");
                photo.delete();
            } catch (Exception e) {
                Log.e(TAG, "Can't create file to take picture!");
                Toast.makeText(ViewMessActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                Error = true;
            }

            if (!Error) {
                mImageUri = Uri.fromFile(photo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                startActivityForResult(intent, CHOOSE_IMAGE_FROM_CAMERA);
            }
        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, CHOOSE_IMAGE_FROM_GALLERY);
        }
        return true;
    }

    private File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG, "onActivityResult()");

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == CHOOSE_IMAGE_FROM_CAMERA || requestCode == CHOOSE_IMAGE_FROM_GALLERY) {

                boolean Error = false;
                Log.i(TAG, "Result: OK, Request:Camera");

                if (requestCode == CHOOSE_IMAGE_FROM_CAMERA)
                    getContentResolver().notifyChange(mImageUri, null);
                else if (requestCode == CHOOSE_IMAGE_FROM_GALLERY)
                    getContentResolver().notifyChange(data.getData(), null);
                ContentResolver cr = this.getContentResolver();
                Bitmap bitmap = null;
                try {
                    if (requestCode == CHOOSE_IMAGE_FROM_CAMERA)
                        bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
                    else if (requestCode == CHOOSE_IMAGE_FROM_GALLERY)
                        bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, data.getData());
                } catch (Exception e) {
                    Toast.makeText(this, "Failed to load image", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Failed to load image");
                    Error = true;
                }

                if (!Error) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                    byte[] byteArray = stream.toByteArray();

                    showProgressBar("Uploading Image...");

                    mParseFile = new ParseFile("mess_photo.jpg", byteArray);
                    mParseFile.saveInBackground(new SaveCallback() {
                        public void done(ParseException e) {
                            progressBar.dismiss();

                            if (e == null) {
                                showProgressDialog("Almost done...");

                                final ParseObject messAsset = new ParseObject("MessAssets");
                                messAsset.put("messCode", messCode);
                                messAsset.put("messName", messName);
                                if (MainScreenActivity.currentUserName != null)
                                    messAsset.put("userName", MainScreenActivity.currentUserName);
                                else messAsset.put("userName", "Anonymous");
                                messAsset.put("isPicture", true);
                                messAsset.put("picture", mParseFile);
                                messAsset.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        progressDialog.dismiss();
                                        if (e == null) {
                                            Toast.makeText(ViewMessActivity.this, "Picture posted", Toast.LENGTH_SHORT).show();
                                            ParseObject logAsset = new ParseObject("Log");
                                            logAsset.put("messCode", messCode);
                                            logAsset.put("messName", messName);
                                            logAsset.put("userName", messAsset.getString("userName"));
                                            logAsset.put("isPicture", true);
                                            logAsset.put("picture", mParseFile);
                                            logAsset.saveInBackground();

                                            photoObjects.add(0, messAsset);
                                            if (photoObjects.size() == 4) photoObjects.remove(3);
                                            displayCommentsAndPhotos(1);
                                        } else {
                                            Log.e(TAG, "Could not save mess asset.Error: " + e.getCode() + " " + e.getMessage());
                                            Toast.makeText(ViewMessActivity.this, "Oops, something went wrong.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            } else {
                                Log.e(TAG, "Could not save parse file. Error: " + e.getMessage() + " " + e.getCode());
                                Toast.makeText(ViewMessActivity.this, "Oops, something went wrong.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new ProgressCallback() {
                        @Override
                        public void done(Integer integer) {
                            Log.i(TAG, "Done: " + integer);
                            progressBar.setProgress(integer);
                        }
                    });
                }
            }
        }
    }

}
