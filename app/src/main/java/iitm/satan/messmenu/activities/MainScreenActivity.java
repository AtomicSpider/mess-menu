package iitm.satan.messmenu.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import iitm.satan.messmenu.MessMenuApp;
import iitm.satan.messmenu.R;
import iitm.satan.messmenu.common.RateTheApp;
import iitm.satan.messmenu.common.Utils;
import iitm.satan.messmenu.fragments.LogFragment;
import iitm.satan.messmenu.fragments.MenuFragment;
import iitm.satan.messmenu.fragments.MessFragment;
import iitm.satan.messmenu.models.MenuObject;

import static iitm.satan.messmenu.common.RateTheApp.setOptOut;

public class MainScreenActivity extends AppCompatActivity {

    private String TAG = "MainScreenActivity";

    //Interface
    private Toolbar mToolbar;
    private android.support.v7.app.ActionBar mActionBar;
    private TabLayout mTabLayout;
    private MainScreenPagerAdapter mMainScreenPagerAdapter;
    private ViewPager mViewPager;
    private ActionBarDrawerToggle mDrawerToggle;

    //Views
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private View mHeaderView;
    private MaterialDialog progressDialog;
    private TextView currentMess, currentUsernameTextView;
    private FloatingActionsMenu menuMultipleActions;
    private FloatingActionButton actionOtherMenu, actionEditMenu, actionViewMess;
    private View positiveAction;

    //Data
    public static List<String> dayArray;
    public static List<String> mealArray;
    private boolean isMessListReady = false, isMenuReady = false, isMessListPinned = false, isMenuPinned = false;
    public static List<ParseObject> messList;
    private List<ParseObject> menuList;
    public static MenuObject currentMenu; // Day, Meal, MessCode, MessName, Main, Extras
    private int dialogMessPos = 0, dialogMealPos = 0, dialogDayPos = 0;
    private String tempUserName = null;
    public static String currentUserName = null;
    private String feedbackDialogSubject, feedbackDialogMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "MainScreenActivity: onCreate()");

        dayArray = Arrays.asList(getResources().getStringArray(R.array.days));
        mealArray = Arrays.asList(getResources().getStringArray(R.array.meals));
        currentUserName = Utils.readSharedPreferences(MessMenuApp.userName);

        setContentView(R.layout.activity_main_screen);
        findViews();

        setUpActionBar();
        setUpViewPager();
        setPageChangeListener();
        setUpNavigationDrawer();
        setDrawerOnClickListeners();
        setUpOnClickListeners();
        setAppRate();
        saveInstallationToParse();

        fetchMessListFromLocalDataStore(false);
        if (Utils.readSharedPreferences(MessMenuApp.isSynced, false)) displayMessMenu(null, null);

        //upLoadMenu();
    }

    private void upLoadMenu() {
        List<ParseObject> tempList = new ArrayList<>();
        tempList.clear();

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < dayArray.size(); j++) {
                ParseObject tempObject = new ParseObject("Menu");
                if (i == 0) {
                    tempObject.put("messObjectId", "2qV90RH73p");
                    tempObject.put("messCode", "mess9");
                    tempObject.put("messName", "Vasan Tiffen SI");
                    tempObject.put("day", dayArray.get(j));
                    tempObject.put("breakfastMain", "Bread, Butter, Jam\n" +
                            "Tea, Coffee, Milk");
                    tempObject.put("breakfastExtras", "N/A");
                    tempObject.put("lunchMain", "N/A");
                    tempObject.put("lunchExtras", "N/A");
                    tempObject.put("dinnerMain", "N/A");
                    tempObject.put("dinnerExtras", "N/A");
                } else {
                    tempObject.put("messObjectId", "Tv1Nvw4Hk2");
                    tempObject.put("messCode", "mess10");
                    tempObject.put("messName", "Cauvery SI");
                    tempObject.put("day", dayArray.get(j));
                    tempObject.put("breakfastMain", "Bread, Butter, Jam\n" +
                            "Tea, Coffee, Milk");
                    tempObject.put("breakfastExtras", "N/A");
                    tempObject.put("lunchMain", "N/A");
                    tempObject.put("lunchExtras", "N/A");
                    tempObject.put("dinnerMain", "N/A");
                    tempObject.put("dinnerExtras", "N/A");
                }

                tempList.add(tempObject);
            }
        }

        ParseObject.saveAllInBackground(tempList, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(MainScreenActivity.this, "Saved", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void setUpOnClickListeners() {
        actionOtherMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                if (messList != null && messList.size() > 0)
                    showMenuKeyChooserDialog();
                else
                    Toast.makeText(MainScreenActivity.this, "Please sync the app", Toast.LENGTH_SHORT).show();
            }
        });

        actionEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();

                if (!Utils.isConnectedToInternet(MainScreenActivity.this))
                    Toast.makeText(MainScreenActivity.this, "This feature requires internet. Please turn on the network", Toast.LENGTH_SHORT).show();
                else {
                    if (currentMenu == null)
                        Toast.makeText(MainScreenActivity.this, "Please sync the app first", Toast.LENGTH_LONG).show();
                    else {
                        if (messList == null) {
                            fetchMessListFromLocalDataStore(true);
                        } else goToEditMenuActivity();
                    }
                }
            }
        });

        actionViewMess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();

                if (!Utils.isConnectedToInternet(MainScreenActivity.this))
                    Toast.makeText(MainScreenActivity.this, "This feature requires internet. Please turn on the network", Toast.LENGTH_SHORT).show();
                else {
                    if (Utils.readSharedPreferences(MessMenuApp.selectedMessCode) == null)
                        Toast.makeText(MainScreenActivity.this, "Please sync the app first", Toast.LENGTH_LONG).show();
                    else {
                        Intent mIntent = new Intent(MainScreenActivity.this, ViewMessActivity.class);
                        mIntent.putExtra("messCode", Utils.readSharedPreferences(MessMenuApp.selectedMessCode));
                        mIntent.putExtra("messName", Utils.readSharedPreferences(MessMenuApp.selectedMessName));
                        startActivity(mIntent);
                        overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom);
                    }
                }
            }
        });
    }

    private void showMenuKeyChooserDialog() {
        ArrayList<String> messDialogArrayList = new ArrayList<>();
        final ArrayList<String> messCodeDialogArrayList = new ArrayList<>();
        messDialogArrayList.clear();
        messCodeDialogArrayList.clear();

        for (int i = 0; i < messList.size(); i++) {
            messDialogArrayList.add(messList.get(i).getString("messName"));
            messCodeDialogArrayList.add(messList.get(i).getString("messCode"));
        }

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Choose Menu")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_menu_chooser, true)
                .positiveText("Show Menu")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        //super.onPositive(dialog);
                        List<String> dialogKey = new ArrayList<String>();
                        dialogKey.clear();
                        dialogKey.add(messCodeDialogArrayList.get(dialogMessPos));
                        dialogKey.add(mealArray.get(dialogMealPos));
                        dialogKey.add(dayArray.get(dialogDayPos));
                        fetchMenuDataAndDisplay(dialogKey);
                    }
                })
                .show();

        View view = dialog.getCustomView();
        Spinner messSpinnerMain = (Spinner) view.findViewById(R.id.messSpinnerMain);
        Spinner mealSpinnerMain = (Spinner) view.findViewById(R.id.mealSpinnerMain);
        Spinner daySpinnerMain = (Spinner) view.findViewById(R.id.daySpinnerMain);

        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, messDialogArrayList);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        messSpinnerMain.setAdapter(mAdapter);

        String currentDialogMessCode = Utils.readSharedPreferences(MessMenuApp.selectedMessCode);

        if (currentDialogMessCode != null) {
            Log.i(TAG, "DialogMenuChooser(): Messcode not null");
            if (messCodeDialogArrayList.indexOf(currentDialogMessCode) != -1)
                messSpinnerMain.setSelection(messCodeDialogArrayList.indexOf(currentDialogMessCode));
            else Log.i(TAG, "Mess not found in list");
        }

        ArrayAdapter<CharSequence> mAdapter1 = ArrayAdapter.createFromResource(this,
                R.array.meals, R.layout.item_spinner);
        mAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mealSpinnerMain.setAdapter(mAdapter1);

        ArrayAdapter<CharSequence> mAdapter2 = ArrayAdapter.createFromResource(this,
                R.array.days, R.layout.item_spinner);
        mAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinnerMain.setAdapter(mAdapter2);

        messSpinnerMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dialogMessPos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mealSpinnerMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dialogMealPos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        daySpinnerMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dialogDayPos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void goToEditMenuActivity() {
        Intent mIntent = new Intent(MainScreenActivity.this, EditMenuActivity.class);
        startActivity(mIntent);
        overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom);
    }

    private void findViews() {
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mHeaderView = mNavigationView.getHeaderView(0);
        currentMess = (TextView) mHeaderView.findViewById(R.id.current_mess);
        currentUsernameTextView = (TextView) mHeaderView.findViewById(R.id.current_username);

        String currentMessString = Utils.readSharedPreferences(MessMenuApp.selectedMessName);
        if (currentMessString != null) currentMess.setText(currentMessString);
        if (currentUserName != null) currentUsernameTextView.setText(currentUserName);

        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        actionOtherMenu = (FloatingActionButton) findViewById(R.id.action_other_menu);
        actionEditMenu = (FloatingActionButton) findViewById(R.id.action_edit_menu);
        actionViewMess = (FloatingActionButton) findViewById(R.id.action_view_mess);
    }

    private void setUpActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setTitle(getResources().getString(R.string.app_name));
    }

    private void setUpViewPager() {
        mMainScreenPagerAdapter = new MainScreenPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mMainScreenPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setTabsFromPagerAdapter(mMainScreenPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    private void setPageChangeListener() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();

                switch (position) {
                    case 0:
                        menuMultipleActions.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        menuMultipleActions.setVisibility(View.GONE);
                        break;
                    case 2:
                        menuMultipleActions.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUpNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d(TAG, "Drawer Opened");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d(TAG, "Drawer Closed");
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    private void setDrawerOnClickListeners() {
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.nav_sync:
                        if (!Utils.isConnectedToInternet(MainScreenActivity.this))
                            Toast.makeText(MainScreenActivity.this, "Network not available", Toast.LENGTH_LONG).show();
                        else
                            syncMessData();
                        break;
                    case R.id.nav_choose_mess:
                        showMessChooserDialog();
                        break;
                    case R.id.nav_set_username:
                        setUsername();
                        break;
                    case R.id.nav_about:
                        showAboutDialog();
                        break;
                    case R.id.nav_share:
                        shareApp();
                        break;
                    case R.id.nav_rate:
                        Intent mIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=iitm.satan.messmenu"));
                        startActivity(mIntent);
                        setOptOut(MainScreenActivity.this, true);
                        break;
                    case R.id.nav_feedback:
                        showFeedbackDialog();
                        break;
                    case R.id.nav_contact:
                        contactIntent();
                        break;
                }

                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    private void contactIntent() {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "iitumstudant@gmail.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Mess Menu App");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        } catch (Exception e) {
            Toast.makeText(MainScreenActivity.this, "Cannot find any Email app", Toast.LENGTH_SHORT).show();
        }

    }

    private void showFeedbackDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Feedback")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_feedback, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        sendFeedback();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);

        final EditText subjectInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_feedback_subject);
        subjectInput.requestFocus();
        final EditText messageInput = (EditText) dialog.getCustomView().findViewById(R.id.dialog_feedback_message);

        subjectInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    feedbackDialogSubject = s.toString().trim();
                    if (messageInput.getText().toString().trim().length() > 0)
                        positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        messageInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    feedbackDialogMessage = s.toString().trim();
                    if (subjectInput.getText().toString().trim().length() > 0)
                        positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);

    }

    private void sendFeedback() {
        showProgressDialog("Sending Feedback...");
        String versionName = "N/A";
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        ParseObject feedback = new ParseObject("Feedback");
        feedback.put("appVersion", versionName);
        feedback.put("subject", feedbackDialogSubject);
        feedback.put("message", feedbackDialogMessage);
        if (currentUserName != null) {
            feedback.put("userName", currentUserName);
        } else {
            feedback.put("userName", "Anonymous");
        }
        feedback.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                if (e == null) {
                    Toast.makeText(MainScreenActivity.this, "Feedback Sent", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainScreenActivity.this, "Oops ! Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void shareApp() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Mess Menu");
            String sAux = "\nYo !! Check out this Mess App\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=iitm.satan.messmenu\n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Share App Via"));
        } catch (Exception e) {
            Log.e(TAG, "Error Sharing App: " + e);
            Toast.makeText(MainScreenActivity.this, "Oops, Cannot share the app", Toast.LENGTH_SHORT).show();
        }
    }

    private void showAboutDialog() {
        new MaterialDialog.Builder(this)
                .title("Features")
                .titleColorRes(R.color.teal_900)
                .customView(R.layout.dialog_about, true)
                .positiveText("Cool")
                .positiveColorRes(R.color.teal_700)
                .show();
    }

    private void setUsername() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Set Username")
                .titleColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_set_username, true)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        if (tempUserName.length() > 15)
                            Toast.makeText(MainScreenActivity.this, "Name cannot exceed 15 characters. Use just your first name maybe?", Toast.LENGTH_SHORT).show();
                        else {
                            currentUserName = tempUserName;
                            currentUsernameTextView.setText(currentUserName);
                            Utils.saveToSharedPreferences(MessMenuApp.userName, tempUserName, false, false);
                            Toast.makeText(MainScreenActivity.this, "Username Saved", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        EditText userNameInput = (EditText) dialog.getCustomView().findViewById(R.id.setUsernameEditText);
        if (currentUserName != null) userNameInput.setText(currentUserName);
        userNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    tempUserName = s.toString().trim();
                    positiveAction.setEnabled(true);
                } else positiveAction.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dialog.show();
        positiveAction.setEnabled(false);
    }

    private void setAppRate() {
        RateTheApp.onStart(this);
        RateTheApp.showRateDialogIfNeeded(this);
    }

    private void saveInstallationToParse() {
        ParseInstallation mParseInstallation = ParseInstallation.getCurrentInstallation();
        if (currentUserName != null)
            mParseInstallation.put("userName", currentUserName);
        else mParseInstallation.put("userName", "Anonymous");
        mParseInstallation.saveInBackground();
    }

    private class MainScreenPagerAdapter extends FragmentPagerAdapter {

        ArrayList<Fragment> fragments;

        public MainScreenPagerAdapter(FragmentManager fm) {
            super(fm);

            fragments = new ArrayList<Fragment>();
            fragments.add(new MenuFragment());
            fragments.add(new MessFragment());
            fragments.add(new LogFragment());
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.menu_fragment);
                case 1:
                    return getResources().getString(R.string.mess_fragment);
                case 2:
                    return getResources().getString(R.string.log_fragment);
                default:
                    return null;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem syncItem = menu.findItem(R.id.action_sync);
        MenuItem currentItem = menu.findItem(R.id.action_current);

        switch (mViewPager.getCurrentItem()) {
            case 0:
                syncItem.setVisible(true);
                currentItem.setVisible(true);
                break;
            case 1:
                syncItem.setVisible(false);
                currentItem.setVisible(false);
                break;
            case 2:
                syncItem.setVisible(false);
                currentItem.setVisible(false);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.action_sync:
                if (!Utils.isConnectedToInternet(MainScreenActivity.this))
                    Toast.makeText(MainScreenActivity.this, "Network not available", Toast.LENGTH_LONG).show();
                else
                    syncMessData();
                break;
            case R.id.action_current:
                displayMessMenu(null, null);
                break;
        }

        return true;
    }

    private void syncMessData() {
        showProgressDialog("Syncing data. Please wait...");

        isMessListReady = false;
        isMenuReady = false;

        ParseQuery<ParseObject> messListQuery = ParseQuery.getQuery("Mess");
        messListQuery.addAscendingOrder("createdAt");
        messListQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> messObjectList, ParseException e) {
                if (e == null) {
                    Log.i(TAG, "Mess list data fetched");
                    isMessListReady = true;
                    messList = new ArrayList<ParseObject>(messObjectList);

                    Log.i(TAG, messList.size() + " Mess fetched");

                    if (isMessListReady && isMenuReady) {
                        pinSyncedData(messList, menuList);
                    }
                } else {
                    Log.i(TAG, "Mess list fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(MainScreenActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ParseQuery<ParseObject> menuQuery = ParseQuery.getQuery("Menu");
        menuQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> menuObjectList, ParseException e) {
                if (e == null) {
                    Log.i(TAG, "Menu data fetched");
                    isMenuReady = true;
                    menuList = new ArrayList<ParseObject>(menuObjectList);

                    Log.i(TAG, menuList.size() + " Menu fetched");

                    if (isMessListReady && isMenuReady) {
                        pinSyncedData(messList, menuList);
                    }
                } else {
                    Log.i(TAG, "Menu fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(MainScreenActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void pinSyncedData(List<ParseObject> messList, List<ParseObject> menuList) {

        isMessListPinned = false;
        isMenuPinned = false;

        MenuFragment.toggleNotSyncedContainerVisibility(false);

        ParseObject.pinAllInBackground(messList, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i(TAG, "Mess List pinned");
                    isMessListPinned = true;

                    if (isMessListPinned && isMenuPinned) {
                        AfterPinStep();
                    }
                } else {
                    progressDialog.dismiss();
                    Log.i(TAG, "Mess List pinning failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(MainScreenActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ParseObject.pinAllInBackground(menuList, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i(TAG, "Menu pinned");
                    isMenuPinned = true;

                    if (isMessListPinned && isMenuPinned) {
                        AfterPinStep();
                    }
                } else {
                    progressDialog.dismiss();
                    Log.i(TAG, "Menu pinning failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(MainScreenActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void AfterPinStep() {
        progressDialog.dismiss();
        Utils.saveToSharedPreferences(MessMenuApp.isSynced, null, true, true);

        displayMessMenu(null, null);
    }

    private void displayMessMenu(String messCode, List<String> key) {
        if (key == null) {
            Log.d(TAG, "displayMessMenu(): key null");
            if (messCode == null) {
                Log.d(TAG, "displayMessMenu(): messCode null");
                String currentMess = Utils.readSharedPreferences(MessMenuApp.selectedMessCode);
                if (currentMess != null) {
                    Log.d(TAG, "displayMessMenu(): currentMess: " + currentMess);
                    fetchMenuDataAndDisplay(getMenuKey(currentMess));
                } else {
                    Log.d(TAG, "displayMessMenu(): currentMess null");
                    showMessChooserDialog();
                }
            } else {
                Log.d(TAG, "displayMessMenu(): messCode: " + messCode);
                fetchMenuDataAndDisplay(getMenuKey(messCode));
            }
        } else {
            Log.d(TAG, "displayMessMenu(): key not null");
            fetchMenuDataAndDisplay(key);
        }
    }

    private void fetchMenuDataAndDisplay(final List<String> key) {
        showProgressDialog("Fetching Menu...");

        ParseQuery<ParseObject> messListLocalQuery = ParseQuery.getQuery("Menu");
        messListLocalQuery.whereEqualTo("messCode", key.get(0));
        messListLocalQuery.whereEqualTo("day", key.get(2));
        messListLocalQuery.fromLocalDatastore();
        messListLocalQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    if (list.size() != 1) {
                        Log.i(TAG, "List Size " + list.size());
                        for (int i = 0; i < list.size(); i++) {
                            Log.i(TAG, "Multiple Menu Found" + list.get(i));
                        }
                        Toast.makeText(MainScreenActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        if (currentMenu == null) currentMenu = new MenuObject();
                        currentMenu.setDay(key.get(2));
                        currentMenu.setMeal(key.get(1));
                        currentMenu.setMessCode(list.get(0).getString("messCode"));
                        currentMenu.setMessName(list.get(0).getString("messName"));

                        if (key.get(1).equals("Breakfast")) {
                            MenuFragment.displayMessData(key.get(2), key.get(1), list.get(0).getString("messName"), list.get(0).getString("breakfastMain"), list.get(0).getString("breakfastExtras"));
                        } else if (key.get(1).equals("Lunch")) {
                            MenuFragment.displayMessData(key.get(2), key.get(1), list.get(0).getString("messName"), list.get(0).getString("lunchMain"), list.get(0).getString("lunchExtras"));
                        } else if (key.get(1).equals("Dinner")) {
                            MenuFragment.displayMessData(key.get(2), key.get(1), list.get(0).getString("messName"), list.get(0).getString("dinnerMain"), list.get(0).getString("dinnerExtras"));
                        }
                    }
                } else {
                    Log.i(TAG, "Local datastore fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(MainScreenActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private List<String> getMenuKey(String currentMess) {
        List<String> key = new ArrayList<>(); // Mess, Meal, Day
        key.add(currentMess);

        String tempString;
        int BTimeFlag = 0;

        Calendar mCalendar = Calendar.getInstance();
        int day = mCalendar.get(Calendar.DAY_OF_WEEK);
        int hour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = mCalendar.get(Calendar.MINUTE);
        if (minute < 10) {
            tempString = "0" + Integer.toString(minute);
        } else tempString = Integer.toString(minute);

        int time = Integer.parseInt((Integer.toString(hour) + tempString));

        if ((time > 2130) | (time < 931)) {
            if (time < 931) BTimeFlag = 0;
            if (time > 2130) BTimeFlag = 1;
            key.add("Breakfast");
        }
        if ((time > 930) & (time < 1431)) {
            key.add("Lunch");
        }
        if ((time > 1430) & (time < 2131)) {
            key.add("Dinner");
        }

        switch (day) {
            case 1:
                if (BTimeFlag == 0) key.add("Sunday");
                else key.add("Monday");
                break;
            case 2:
                if (BTimeFlag == 0) key.add("Monday");
                else key.add("Tuesday");
                break;
            case 3:
                if (BTimeFlag == 0) key.add("Tuesday");
                else key.add("Wednesday");
                break;
            case 4:
                if (BTimeFlag == 0) key.add("Wednesday");
                else key.add("Thursday");
                break;
            case 5:
                if (BTimeFlag == 0) key.add("Thursday");
                else key.add("Friday");
                break;
            case 6:
                if (BTimeFlag == 0) key.add("Friday");
                else key.add("Saturday");
                break;
            case 7:
                if (BTimeFlag == 0) key.add("Saturday");
                else key.add("Sunday");
                break;
        }

        Log.i(TAG, key.get(0) + " " + key.get(1) + " " + key.get(2));
        return key;
    }

    private void showMessChooserDialog() {
        if (!Utils.readSharedPreferences(MessMenuApp.isSynced, false))
            Toast.makeText(MainScreenActivity.this, "App not synced. Please sync the app to download mess data", Toast.LENGTH_LONG).show();
        else {
            if (messList == null) {
                fetchMessListFromLocalDataStoreAndShowDialog();
            } else {
                CharSequence messStringList[] = new String[messList.size()];

                for (int i = 0; i < messList.size(); i++) {
                    messStringList[i] = messList.get(i).getString("messName");
                }

                boolean cancelable;
                if (Utils.readSharedPreferences(MessMenuApp.selectedMessCode) == null)
                    cancelable = false;
                else cancelable = true;

                new MaterialDialog.Builder(this)
                        .title("Choose Mess")
                        .titleColorRes(R.color.colorPrimaryDark)
                        .itemColorRes(R.color.colorPrimary)
                        .cancelable(cancelable)
                        .items(messStringList)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                Utils.saveToSharedPreferences(MessMenuApp.selectedMessCode, messList.get(which).getString("messCode"), false, false);
                                Utils.saveToSharedPreferences(MessMenuApp.selectedMessName, messList.get(which).getString("messName"), false, false);
                                Log.e("errorMess",messList.get(which).getString("messName"));
                                currentMess.setText(messList.get(which).getString("messName"));
                                displayMessMenu(messList.get(which).getString("messCode"), null);
                            }
                        })
                        .show();
            }
        }
    }

    private void fetchMessListFromLocalDataStoreAndShowDialog() {

        showProgressDialog("Fetching Mess Halls...");

        ParseQuery<ParseObject> messListLocalQuery = ParseQuery.getQuery("Mess");
        messListLocalQuery.whereEqualTo("active", true);
        messListLocalQuery.addAscendingOrder("createdAt");
        messListLocalQuery.fromLocalDatastore();
        messListLocalQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    Log.i(TAG, list.size() + " messes found");
                    if (messList == null) messList = new ArrayList<ParseObject>();
                    messList = list;
                    showMessChooserDialog();
                } else {
                    Log.i(TAG, "Local datastore fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                    Toast.makeText(MainScreenActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void fetchMessListFromLocalDataStore(final boolean goToEditMenuActivity) {

        if (goToEditMenuActivity) showProgressDialog("Fetching Mess List...");

        final ParseQuery<ParseObject> messListLocalQuery = ParseQuery.getQuery("Mess");
        messListLocalQuery.whereEqualTo("active", true);
        messListLocalQuery.addAscendingOrder("createdAt");
        messListLocalQuery.fromLocalDatastore();
        messListLocalQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (goToEditMenuActivity) progressDialog.dismiss();

                if (e == null) {
                    Log.i(TAG, list.size() + " messes found");
                    if (messList == null) messList = new ArrayList<ParseObject>();
                    messList = list;
                    if (goToEditMenuActivity) goToEditMenuActivity();
                } else {
                    Log.i(TAG, "Local datastore fetch failed. Error: " + e.getMessage() + "ErrorCode: " + e.getCode());
                }
            }
        });
    }

    private void showProgressDialog(String message) {
        progressDialog = new MaterialDialog.Builder(this)
                .content(message)
                .contentColorRes(R.color.colorPrimaryDark)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }
}
