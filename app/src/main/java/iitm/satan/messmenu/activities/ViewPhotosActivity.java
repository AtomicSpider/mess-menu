package iitm.satan.messmenu.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import iitm.satan.messmenu.R;
import iitm.satan.messmenu.common.SquareImageView;
import iitm.satan.messmenu.models.DataPager;

/**
 * Created by Sanat Dutta on 8/31/2015.
 */
public class ViewPhotosActivity extends AppCompatActivity {

    private String TAG = "ViewPhotosActivity";

    //Interfaces
    private photosAdapter mAdapter;
    private ImageLoader mImageLoader;

    //Views
    private GridView mGridView;
    private CircularProgressView fetchingDataProgressBar, fetchingDataProgressBarBelow;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    //Data
    private String messCode, messName;
    private boolean isDataFetching = false, isEverythingLoaded = false;
    private int PHOTOS_PER_PAGE = 18;
    private Date lastLoadedDate = null;
    private ArrayList<ParseObject> photoObjects = new ArrayList<>();
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "ViewPhotosActivity: onCreateView");
        setContentView(R.layout.activity_view_photos);

        buildImageLoaderOptions();

        getIntentData();
        findViews();
        setUpActionBar();
        mAdapter = new photosAdapter();
        mGridView.setAdapter(mAdapter);
        setUpSwipeRefreshLayout();
        getPhotos(null);


    }

    private void buildImageLoaderOptions() {
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placeholder_image)
                .showImageForEmptyUri(R.drawable.placeholder_image)
                .showImageOnFail(R.drawable.placeholder_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    private void setUpActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(messName + " Photos");
    }

    private void getIntentData() {
        messCode = getIntent().getStringExtra("messCode");
        messName = getIntent().getStringExtra("messName");
    }

    private void getPhotos(final Date mDate) {
        isDataFetching = true;

        Log.i(TAG, "Fetching Photos");
        ParseQuery<ParseObject> photoObjectsQuery = new ParseQuery("MessAssets");
        photoObjectsQuery.setLimit(PHOTOS_PER_PAGE);
        photoObjectsQuery.addDescendingOrder("createdAt");
        photoObjectsQuery.whereEqualTo("messCode", messCode);
        photoObjectsQuery.whereEqualTo("isPicture", true);
        if (mDate != null) photoObjectsQuery.whereLessThan("createdAt", mDate);
        photoObjectsQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                fetchingDataProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                if (e == null) {
                    if (list.size() > 0) {
                        Log.i(TAG, "Photos Fetching Finished");
                        mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.white));
                        if (mDate == null) photoObjects.clear();
                        photoObjects.addAll(list);
                        lastLoadedDate = photoObjects.get(photoObjects.size() - 1).getCreatedAt();
                        mAdapter.notifyDataSetChanged();
                        fetchingDataProgressBarBelow.setVisibility(View.GONE);
                    } else {
                        if (mDate == null) {
                            Log.e(TAG, "No Recent Photos");
                            photoObjects.clear();
                            mAdapter.notifyDataSetChanged();
                            fetchingDataProgressBarBelow.setVisibility(View.GONE);
                            mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
                        } else {
                            isEverythingLoaded = true;
                            mAdapter.notifyDataSetChanged();
                            fetchingDataProgressBarBelow.setVisibility(View.GONE);
                        }
                    }
                } else {
                    Log.e(TAG, "Unable to fetch Photos. Error: " + e);
                    Toast.makeText(ViewPhotosActivity.this, "Oops, Something went wrong", Toast.LENGTH_SHORT).show();
                }
                isDataFetching = false;
            }
        });
    }

    private void setUpSwipeRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "onRefresh");
                if (!isDataFetching) {
                    isEverythingLoaded = false;
                    lastLoadedDate = null;
                    getPhotos(lastLoadedDate);
                } else {
                    Log.i(TAG, "Data Fetch Ongoing");
                }
            }
        });
    }

    private void findViews() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.photos_swipe_refresh_layout);
        mGridView = (GridView) findViewById(R.id.photosGridView);
        fetchingDataProgressBar = (CircularProgressView) findViewById(R.id.fetchingDataProgressBar);
        fetchingDataProgressBarBelow = (CircularProgressView) findViewById(R.id.fetchingDataProgressBarBelow);
    }

    private class photosAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;

        private photosAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return photoObjects.size();
        }

        @Override
        public Object getItem(int position) {
            return photoObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if ((position == (photoObjects.size() - 1)) && (photoObjects.size() > PHOTOS_PER_PAGE - 1) && !isEverythingLoaded)
                fetchingDataProgressBarBelow.setVisibility(View.VISIBLE);
            ;

            View mView = mLayoutInflater.inflate(R.layout.item_photo_grid, parent, false);
            ViewHolder mViewHolder = new ViewHolder();

            mViewHolder.mImage = (SquareImageView) mView.findViewById(R.id.picture);

            ImageLoader.getInstance().displayImage(photoObjects.get(position).getParseFile("picture").getUrl(), mViewHolder.mImage, options);

            mViewHolder.mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enlargePhoto(position);
                }
            });

            // Fetch more data
            if ((position == (photoObjects.size() - 1)) && !isDataFetching && !isEverythingLoaded)
                getPhotos(lastLoadedDate);

            return mView;
        }

        private class ViewHolder {
            private SquareImageView mImage;
        }
    }

    private void enlargePhoto(int position) {
        ArrayList<DataPager> imageUriList = new ArrayList<>();
        imageUriList.clear();

        for (int i = 0; i < photoObjects.size(); i++) {
            imageUriList.add(new DataPager(photoObjects.get(i).getParseFile("picture").getUrl()));
        }

        Intent mIntent = new Intent(ViewPhotosActivity.this, PhotoEnlargeActivity.class);
        mIntent.putExtra("imageUris", imageUriList);
        mIntent.putExtra("position", position);
        startActivity(mIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

