package iitm.satan.messmenu;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

import java.io.File;

/**
 * Created by Sanat Dutta on 8/28/2015.
 */
public class MessMenuApp extends Application {

    private String TAG = "MessMenuApp";

    //Data
    public static SharedPreferences mSharedPreferences;
    public static String isSynced = "IS_SYNCED";
    public static String selectedMessCode = "SELECTED_MESS_CODE";
    public static String selectedMessName = "SELECTED_MESS_NAME";
    public static String userName = "USER_NAME";

    public static File EXTERNAL_CACHE_DIR;

    @Override
    public void onCreate() {
        super.onCreate();

        mSharedPreferences = getSharedPreferences("MESS_MENU_PREF", Context.MODE_PRIVATE);
        EXTERNAL_CACHE_DIR = getExternalCacheDir();

        //initialize Parse with Parse Server
        //Parse.initialize(this, getResources().getString(R.string.parse_app_id), getResources().getString(R.string.parse_client_id));

        //initialize Parse with Heroku Server
        Parse.initialize(new Parse.Configuration.Builder(this)
                        .applicationId(getResources().getString(R.string.parse_app_id))
                        .clientKey(getResources().getString(R.string.parse_client_id))
                        .server("https://parseservermessmenu.herokuapp.com/parse").enableLocalDataStore().build()
        );

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

        initImageLoader(getApplicationContext());


        //ToDo Add internet no not open
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
}
